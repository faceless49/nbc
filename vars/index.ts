export const API_URL = 'https://jsonplaceholder.typicode.com/';

export const GUTTER_COLUMN = 0;
export const GUTTER_ROW = 0;
export const LIMIT_PHOTOS = 20;
export const NEXT_COUNT_VALUE = 10;
